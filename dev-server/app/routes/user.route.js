const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user.controller');

router.route('/api/users')
  .get(userCtrl.getName)
  .post(userCtrl.setName)
  .put(userCtrl.updateName)
  .delete(userCtrl.deleteName);

module.exports = router;
