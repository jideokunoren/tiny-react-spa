require('../../../get_config_settings');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const userRoute = require('../routes/user.route');
app.use('/', userRoute);

module.exports = app.listen(process.env.PORT,
  console.info(
    `Server is up & running at ${process.env.PROJECT_DOMAIN}:${process.env.PORT}`
  ));
