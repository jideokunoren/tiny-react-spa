let userName = '';

exports.getName = (req,res) => {

  if(!userName) {
    return res.status(404).json({ message: "NOT FOUND"})
  }
  res.status(200).json({ name: userName});
};

exports.setName = (req,res) => {
  if(!req.body.name) {
    return res.status(404).json({ message: "NAME IS REQUIRED"})
  }
  userName = req.body.name;
  res.status(200).json({ name: userName});
};

exports.updateName = (req,res) => {
  if(!req.body.name) {
    return res.status(404).json({ message: "NAME IS REQUIRED"})
  }
  userName = req.body.name;
  res.status(200).json({ name: userName});
}


exports.deleteName = (req,res) => {

  userName = null;
  res.status(200).json({result: "Name deleted..."});
}

