# Tiny React SPA

This is a very simple React Single Page Application that allows a user to create, view & modify their name (or a string of text).

## Overview
The entire app consists of a React SPA & a Node based web service that presents an API. (Why Node? Mainly for running a quick integration test suite & managing the entire process).


## Installation

```
  Just run npm install
```

## Running the App

* To run the app with a server - npm run start

* To run the app standalone - npm run start-web (Be aware that this will run the SPA, you will need to
  modify line 8 const API_DOMAIN = 'http://localhost:8080/api/users' of
  "tiny-react-spa/src/containers/SimpleForm/index.js")

* To build the app - npm run build (tiny-react-spa/dist already exists)

* To run the tests - stop the entire app & server if running then npm run test.
   This will spin up the server, the SPA & then the Cypress test suite. This is an Electron interface that
   performs integration tests.

## Notes
It was not possible to use the @instructure/canvas-theme library due to multiple incompatibility issues with
React & Webpack versions. Bootstrap was used for quick theming.





