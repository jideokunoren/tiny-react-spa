import React from 'react';
import PropTypes from 'prop-types';
import {Form} from "react-bootstrap";


const InputBox = ({name, textInputData, callback}) => (
  <>
    <Form.Control name="user-name"
                  type="text"
                  id="user-name"
                  value={textInputData ? textInputData : ''}
                  placeholder={name}
                  onChange={callback}/>
  </>
);

InputBox.propTypes = {
  name: PropTypes.string,
  textInputData: PropTypes.string,
  callback: PropTypes.func
}
export default InputBox;