import React from 'react';
import PropTypes from 'prop-types';
import {Button} from "react-bootstrap";

const SubmitButton =  ({buttonMessage,buttonType,textInputData}) => (

    <Button
            type="submit"
            id="submit-button"
            variant={buttonType}
            disabled={!textInputData}>
      {buttonMessage}
    </Button>

);

SubmitButton.propTypes = {
    buttonMessage: PropTypes.string,
    buttonType: PropTypes.string,
    textInputData: PropTypes.string
}
export default SubmitButton;