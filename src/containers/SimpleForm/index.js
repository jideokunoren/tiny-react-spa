import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Form} from "react-bootstrap";
import InputBox from "../../components/InputBox";
import SubmitButton from "../../components/SubmitButton";
import '../../components/InputBox';

const API_DOMAIN = 'http://localhost:8080/api/users';
 const fetchSettings = {
   headers: {
     Accept: 'application/json',
     'Content-Type': 'application/json',
     method: 'GET'
   }
 };

class SimpleForm extends Component {
  constructor() {
    super();
    this.state = {
      name: null,
      buttonMessage: null,
      buttonType: 'primary',
      error: null,
      textInputData: ''
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  async componentDidMount() {
    try {
      fetchSettings.method = 'GET';
      const userName = await fetch(API_DOMAIN,fetchSettings);
      const userNameJSON = await userName.json();
      if(userNameJSON.name) {
        this.setState({
          name: userNameJSON.name,
          buttonMessage:'Update'
        })
      } else {
        this.setState({
          buttonMessage:'Save'
        })
       }
    } catch(err) {
      this.setState({
        error: err.message
      })
    }
  }


  async handleSubmit (event) {
    event.preventDefault();
   try {
    let {name, buttonMessage,textInputData} = this.state;
    if (!name && buttonMessage === 'Save' && textInputData.length) {
      fetchSettings.method = 'POST';
    } else {
      fetchSettings.method = 'PUT';
    }
      fetchSettings.body = JSON.stringify({"name": textInputData});

       const results = await fetch(API_DOMAIN,fetchSettings);
       const resultsJSON = await results.json();

      if (resultsJSON.name) {
        this.setState({
          name: resultsJSON.name,
          buttonMessage: 'Update',
          textInputData:''
        })
      }

    } catch(err) {
      this.setState({
        error: err.message
      })
    }
  }

  async handleDelete (event) {
    event.preventDefault();
      fetchSettings.method = 'DELETE';
      const results = await fetch(API_DOMAIN,fetchSettings);
      const resultsJSON = await results.json();

      if (resultsJSON.name) {
        this.setState({
          name: null,
          buttonMessage: null,
          buttonType: 'primary',
          error: null,
          textInputData: ''
        })
      }

    } catch(err) {
      this.setState({
        error: err.message
      })
    }

  async handleUpdate (event) {
    event.preventDefault();
    if(event.target.value) {
      this.setState({
        textInputData: event.target.value,
        error: null,
      });
    }
  }

  render() {
    let { name,error, buttonMessage,buttonType,textInputData} = this.state;
    return  <React.Fragment>
      <Container>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="formBasicNameUpdate">
            <Form.Label>Name</Form.Label>
              <InputBox name={name} textInputData={textInputData} callback={this.handleUpdate}/>
          </Form.Group>
          <SubmitButton buttonMessage={buttonMessage} buttonType={buttonType} textInputData={textInputData} />
        </Form>
        <label>
          {error}
        </label>
      </Container>
   </React.Fragment>
  }
}

export default SimpleForm;