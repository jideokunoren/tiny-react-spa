/**
 *  Quick helper for validating config settings -
 * for development, using a .env file is best for quick customisation
 */

let dotEnvExpand;
let dotEnvConfig;

const expected_env_variables = {
  NODE_ENV:"NODE ENVIRONMENT",
  PROJECT_DOMAIN:"PROJECT DOMAIN",
  PORT:"HOST PORT",
};

if(process.env.NODE_ENV==='development' || !process.env.NODE_ENV) {
  dotEnvConfig = require('dotenv').config();
  dotEnvExpand = require('dotenv-expand');
}


for(const [key,value] of Object.entries(expected_env_variables)) {
  if(!process.env.hasOwnProperty(key))
    throw Error(`${value} HAS NOT BEEN DEFINED!`);
}


module.exports =  dotEnvExpand(dotEnvConfig);