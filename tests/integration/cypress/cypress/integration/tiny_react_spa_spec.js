const nameEntry = require('../fixtures/tiny_react_spa_test_name');

describe('Tiny React SPA tests', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8000/')
  });

  describe('On app & server startup, no name exists on the server,' +
    'so the app should not be able to retrieve a name', () => {
    it('Text input should be empty when no name is retrieved from the server', () => {
      cy.get('#user-name').should('be.empty');
      cy.get('#submit-button').contains('Save').should('be.disabled')
    });

    describe('Create & save a name', () => {
      it('When the input field has been populated & the save button clicked,' +
        ' the input field\'s placeholder should be populated with the new name ' +
        '& the Submit button should be disabled & display the text "Update"', () => {
        cy.get('#user-name').type(nameEntry.name);
        cy.get('#submit-button').click();

        cy.get('#submit-button').contains('Update').should('be.disabled');
        cy.get('#user-name').should('have.attr', 'placeholder', nameEntry.name);
      });

      describe('Update & save a name', () => {
      it('When the name has been amended & the "Update button clicked",' +
        ' the input field\'s placeholder should be populated with the updated name ' +
        '& the Submit button should be disabled & display the text "Update"', () => {
        cy.get('#user-name').type(nameEntry.updated_name);
        cy.get('#submit-button').click();

        cy.get('#submit-button').contains('Update').should('be.disabled');
        cy.get('#user-name').should('have.attr', 'placeholder', nameEntry.updated_name);
    });
  });
  });
});
});
