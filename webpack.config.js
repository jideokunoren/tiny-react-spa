const dotenv = require('dotenv');
const path = require('path');
const webpack = require('webpack');
const envVars = dotenv.config().parsed;
const {  DOMAIN,ENDPOINT } =  dotenv.config().parsed;
const API_DOMAIN = `${DOMAIN}/${ENDPOINT}`;

const HtmlWebpackPlugin = require('html-webpack-plugin');
const htmlWebpackPlugin = new HtmlWebpackPlugin({
  filename: './index.html',
  template: './src/index.html'
});

module.exports = {

  entry: {
    index: './src/index.js',
  },
  output:{
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules:[
      {
        test:/\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader','eslint-loader']
      },
      {
        test:/\.css$/,
        use:['style-loader','css-loader']
      }
    ]
  },
  plugins:[
    htmlWebpackPlugin,
    new webpack.DefinePlugin(envVars)
  ]
};